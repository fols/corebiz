import React from 'react'
import SliderMain from '../../components/SliderMain/'
import ProductLists from '../../components/ProductLists/'
import FormNewsletter from '../../components/Newsletter/'

const Home = () => {
  return (
    <div className="page home">
      <SliderMain />
      <ProductLists />
      <FormNewsletter />
    </div>
  )
}

export default Home
