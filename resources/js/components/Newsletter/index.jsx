import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { validateString, validateEmail } from '../../helpers/validate'
import './styles.scss'
import { FaSpinner } from 'react-icons/fa'
import { apiCorebiz } from '../../context'

const FormNewsletter = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm()

  const [loading, setLoading] = useState(false)
  const [sendSuccess, setSendSuccess] = useState(false)

  const onSubmit = async (data) => {
    setLoading(true)
    const response = await apiCorebiz.post('/newsletter', data)
    const { status, data: dataResponse } = response
    if (status === 200 && dataResponse) {
      setSendSuccess(true)
      reset(data)
    }

    setLoading(false)
  }

  return (
    <div className="formNewsletter">
      <div className="formNewsletterContent">
        {!sendSuccess && (
          <>
            <p className="formNewsletter--text">
              <strong>
                Participe de nossas news com promoções e novidades!
              </strong>
            </p>
            <form
              className="formNewsletterForm"
              id="frmNewsletter"
              onSubmit={handleSubmit(onSubmit)}
            >
              <div className="formNewsletterInputGroup">
                <input
                  type="text"
                  className={`formNewsletterInput name ${
                    errors.name ? 'formNewsletterInputError' : ''
                  }`}
                  id="frmNewsName"
                  placeholder="Digite seu nome"
                  {...register('name', {
                    required: 'Preencha com seu nome completo',
                    validate: (value) =>
                      validateString(value) ||
                      'Preencha com seu nome completo válido',
                    minLength: {
                      value: 3,
                      message: '',
                    },
                    maxLength: {
                      value: 150,
                      message: '',
                    },
                  })}
                />
                {errors.name && (
                  <span className="formNewsletterTextError" role="alert">
                    {errors.name.message}
                  </span>
                )}
              </div>
              <div className="formNewsletterInputGroup">
                <input
                  type="email"
                  id="email"
                  className={`formNewsletterInput email ${
                    errors.email ? 'formNewsletterInputError' : ''
                  }`}
                  placeholder="Digite seu email"
                  {...register('email', {
                    required: 'Preencha com um e-mail',
                    validate: (value) =>
                      validateEmail(value) || 'Preencha com um e-mail válido',
                  })}
                />
                {errors.email && (
                  <span className="formNewsletterTextError" role="alert">
                    {errors.email.message}
                  </span>
                )}
              </div>
              <div className="formNewsletterBottom">
                <button
                  className={`formNewsletterBtn ${loading ? 'sending' : ''} ${
                    errors.name ? 'error' : ''
                  }`}
                >
                  {loading && (
                    <span className="loadingBox">
                      <FaSpinner
                        size="30px"
                        className="iconLoading"
                        color="currentColor"
                      />
                    </span>
                  )}
                  Eu quero
                </button>
              </div>
            </form>
          </>
        )}
        {sendSuccess && (
          <div className="formNewsletterSuccess">
            <p className="formNewsletter--text">
              <strong>Seu e-mail foi cadastrado com sucesso!</strong>
              <br />A partir de agora você receberá as novidade e ofertas
              exclusivas.
            </p>
            <div className="formNewsletterNewEmail">
              <button
                className="formNewsletterBtn"
                onClick={() => setSendSuccess(false)}
              >
                Cadastrar novo e-mail
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default FormNewsletter
