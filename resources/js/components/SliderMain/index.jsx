import React from 'react'
import TinySlider from 'tiny-slider-react'
import 'tiny-slider/dist/tiny-slider.css'

import './styles.scss'

const imagesList = [
  {
    imageDesktop: '/assets/images/Banner-01-desk.jpg',
    imageMobile: '/assets/images/Banner-01-mob.jpg',
    alt: 'Banner 1',
    link: '',
  },
  {
    imageDesktop: '/assets/images/Banner-01-desk.jpg',
    imageMobile: '/assets/images/Banner-01-mob.jpg',
    alt: 'Bannerr 2',
    link: '',
  },
  {
    imageDesktop: '/assets/images/Banner-01-desk.jpg',
    imageMobile: '/assets/images/Banner-01-mob.jpg',
    alt: 'Bannerr 2',
    link: '',
  },
]

const settings = {
  items: 1,
  lazyload: true,
  slideBy: 'page',
  mouseDrag: true,
  swipeAngle: false,
  speed: 300,
  controls: false,
  navPosition: 'bottom',
}

const SliderMain = () => {
  return (
    <div className="sliderMain">
      <TinySlider settings={settings}>
        {imagesList &&
          imagesList.map((item, index) => {
            const { imageDesktop, imageMobile, alt } = item
            return (
              <div key={`image-${index}`} className="sliderMainItem">
                <picture className="sliderMainItemPicture">
                  <source media="(max-width: 768px)" srcSet={imageMobile} />
                  <source media="(min-width: 769px)" srcSet={imageDesktop} />
                  <img
                    src={imageDesktop}
                    alt={alt}
                    className="sliderMainItemImage tns-lazy-img"
                  />
                </picture>
              </div>
            )
          })}
      </TinySlider>
    </div>
  )
}

export default SliderMain
