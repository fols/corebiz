import React from 'react'
import PropTypes from 'prop-types'
import './styles.scss'

const Rating = ({ starActive = 0 }) => {
  const numberStar = 5

  return (
    <div className="rating">
      <div className="ratingContent">
        {Array.from({ length: numberStar }).map((i, index) => {
          const fillColor = index + 1 <= starActive ? '#F8475F' : '#FFF'
          const active = index + 1 <= starActive ? 'active' : ''

          return (
            <div key={index} className={`ratingStar ${active}`}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill={fillColor}
                stroke="#F8475F"
                strokeWidth="2"
                className={`ratingStar--caret ${active}`}
              >
                <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
              </svg>
            </div>
          )
        })}
      </div>
    </div>
  )
}

Rating.propTypes = {
  starActive: PropTypes.number,
}

export default Rating
