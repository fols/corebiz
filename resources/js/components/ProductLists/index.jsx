import React, { useEffect, useState } from 'react'
import TinySlider from 'tiny-slider-react'
import 'tiny-slider/dist/tiny-slider.css'
import Rating from './components/Ratings'
import { apiCorebiz, useTestContext } from '../../context'

import { formatValue } from '../../helpers/utils'
import './styles.scss'

const settings = {
  items: 4,
  lazyload: true,
  slideBy: 'page',
  mouseDrag: true,
  swipeAngle: false,
  speed: 300,
  controls: false,
  navPosition: 'bottom',
  gutter: 12,
  edgePadding: 12,
  loop: false,
  responsive: {
    320: {
      items: 2,
    },
    768: {
      items: 3,
    },
    1120: {
      items: 4,
      gutter: 99,
    },
  },
}

const ProductLists = () => {
  const [loading, setLoading] = useState(true)
  const [products, setProducts] = useState()
  const { addToCart } = useTestContext()

  const getProducts = async () => {
    try {
      const response = await apiCorebiz.get('products')
      const { status, data } = response

      if (status === 200) {
        setProducts(data)
        setLoading(false)
      }
    } catch (e) {
      console.log('error', e)
      setLoading(false)
    }
  }

  const getHighestInstallments = (installments) => {
    var values = installments.map((val) => val.quantity)
    var max = Math.max.apply(null, values)
    const plots = installments.filter((installment) => {
      if (installment.quantity === max) {
        return installment
      }
    })

    return plots[0] || []
  }

  useEffect(() => {
    if (!products && loading) {
      getProducts()
    }
  }, [products])

  if (loading) {
    return <></>
  }

  return (
    <div className="productList content">
      <div className="productListContent">
        {products && (
          <>
            <h3 className="productListTitle titleArea">Mais vendidos</h3>
            <div className="productListItems">
              <TinySlider settings={settings}>
                {products.map((product, index) => {
                  const {
                    imageUrl,
                    installments,
                    listPrice,
                    price,
                    productId,
                    productName,
                    stars,
                  } = product
                  const installment = getHighestInstallments(installments)

                  const { quantity: qttInstalments, value: valueInstallments } =
                    installment

                  return (
                    <div
                      key={`image-${index}`}
                      className="productListItem"
                      data-id={productId}
                    >
                      <div className="productListItem--picture">
                        {listPrice && (
                          <span className="productListItem--discount"></span>
                        )}
                        <img
                          src={imageUrl}
                          alt={productName}
                          className="productListItem--picture-image"
                        />
                      </div>
                      <div className="productListItem--info">
                        <div className="productListItem--name">
                          {productName}
                        </div>
                        <div className="productListItem--stars">
                          <Rating starActive={stars} />
                        </div>
                        <div className="productListItem--prices">
                          <div className="productListItem--prices-listPrice">
                            {listPrice > 0 && (
                              <span className="productListItem--prices-listPrice-value">
                                de {formatValue(listPrice, 'vtex')}
                              </span>
                            )}
                          </div>
                          <div className="productListItem--prices-bestPrice">
                            <span className="productListItem--prices-bestPrice-value">
                              por {formatValue(price, 'vtex')}
                            </span>
                          </div>
                          <div className="productListItem--prices-installments">
                            {installments.length > 0 && (
                              <span className="productListItem--prices-installments-value">
                                ou em {qttInstalments}x de{' '}
                                {formatValue(valueInstallments, 'vtex')}
                              </span>
                            )}
                          </div>
                        </div>
                        <div className="productListItem--Button">
                          <a
                            title={productName}
                            className="productListItem--link"
                            onClick={(e) => addToCart(e, productId, product)}
                          >
                            Comprar
                          </a>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </TinySlider>
            </div>
          </>
        )}
      </div>
    </div>
  )
}

export default ProductLists
